#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

class Matrices{
    // Se crea la clase 'Matrices'
    private: // Atributos
        float **Matriz1;
        float **Matriz2;

    public: // Métodos
        int Filas;
        int Columnas;

        Matrices(int, int); // Constructor de la clase
        void CrearMatrices();
        void MostrarMatrices(); // Función para mostrar matriz
        void SumarMatrices();
        void RestarMatrices();
        void MultMatrices();
};

void Probar_Matrices(int F, int C){
    Matrices NuevaMatriz(F, C);
    NuevaMatriz.CrearMatrices();
    NuevaMatriz.MostrarMatrices();
    NuevaMatriz.SumarMatrices();
    NuevaMatriz.RestarMatrices();
    NuevaMatriz.MultMatrices();
}

Matrices::Matrices(int _Filas, int _Columnas){
    // Constructor de la clase 'Matrices'
    Filas = _Filas;
    Columnas = _Columnas;
}

void Matrices::CrearMatrices(){

    srand((signed)time(NULL));

    Matriz1 = new float *[Filas];
    Matriz2 = new float *[Filas];

    for(int i=0; i<Filas; i++){
      Matriz1[i] = new float[Columnas];
      Matriz2[i] = new float[Columnas];

      for(int j=0; j<Columnas; j++){
        Matriz1[i][j] = -100 + rand()%(201);
        Matriz2[i][j] = -100 + rand()%(201);
      }
    }
}

void Matrices::MultMatrices(){
    float Suma;
    cout << endl << endl << "\t\tProducto de Matrices (M1 * M2')\n";
    for(int i=0; i<Filas; i++){
        cout << endl << "\t";
      for(int j=0; j<Filas; j++){
        Suma = 0;
        for(int k=0; k<Columnas; k++){
            Suma += Matriz1[i][k]*Matriz2[j][k];
        }
        cout << Suma << "\t";
      }
    }
    cout << endl << endl;
}

void Matrices::SumarMatrices(){

    cout << endl << endl << "\t\tSuma de Matrices (M1 + M2)\n";
    for(int i=0; i<Filas; i++){
        cout << endl << "\t";
      for(int j=0; j<Columnas; j++){
        cout << Matriz1[i][j]+Matriz2[i][j] << "\t";
      }
    }
    cout << endl << endl;
}

void Matrices::RestarMatrices(){

    cout << endl << endl << "\t\tResta de Matrices (M1 - M2)\n";
    for(int i=0; i<Filas; i++){
        cout << endl << "\t";
      for(int j=0; j<Columnas; j++){
        cout << Matriz1[i][j]-Matriz2[i][j] << "\t";
      }
    }
    cout << endl << endl;
}

void Matrices::MostrarMatrices(){

    cout << endl << endl << "\t\tMatriz 1\n";
    for(int i=0; i<Filas; i++){
        cout << endl << "\t";
      for(int j=0; j<Columnas; j++){
        cout << Matriz1[i][j] << "\t";
      }
    }
    cout << endl << endl << "\t\tMatriz 2\n";

    for(int i=0; i<Filas; i++){
        cout << endl << "\t";
      for(int j=0; j<Columnas; j++){
        cout << Matriz2[i][j] << "\t";
      }
    }
    cout << endl << endl;
}

